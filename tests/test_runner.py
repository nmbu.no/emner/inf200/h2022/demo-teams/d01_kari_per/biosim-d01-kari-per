from biosim.runner import Runner

def test_move():
    st = 5
    r = Runner(5)
    r.move()
    assert r.pos == st + 1

def test_jump():
    st = 5
    r = Runner(5)
    r.move()
    assert r.pos == st + 3